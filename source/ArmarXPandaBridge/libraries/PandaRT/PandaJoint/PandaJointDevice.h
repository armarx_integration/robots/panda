/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXSimulation::ArmarXObjects::RobotUnitSimulation
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// ArmarX
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/ControlModes.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>


namespace pandax
{
    TYPEDEF_PTRS_SHARED(PandaJointDevice);

    class PandaJointDevice :
        virtual public armarx::SensorDeviceTemplate<armarx::SensorValue1DoFActuator>,
        virtual public armarx::ControlDevice
    {

        static const float nullFloat;

    public:

        struct PandaJointControllerBase :
            public armarx::JointController
        {
            armarx::NameValueMap* map;
            const float* val;
            const float* sensVal;
            float* mapVal;
            void rtRun(const IceUtil::Time&, const IceUtil::Time&) override;
        };

        template<class TargT>
        struct PandaJointController :
            public PandaJointControllerBase
        {
            PandaJointController(std::string hwMode) : hwMode {hwMode} {}

            TargT targ;
            const std::string hwMode;
            armarx::ControlTargetBase* getControlTarget() override
            {
                return &targ;
            }
            std::string getHardwareControlMode() const override
            {
                return hwMode;
            }
        };

        PandaJointDevice(const std::string& name, armarx::NameValueMap& ctrlpos, armarx::NameValueMap& ctrlvel, armarx::NameValueMap& ctrltor);
        void rtSetActiveJointController(armarx::JointController* jointCtrl) override;

        PandaJointController<armarx::ControlTarget1DoFActuatorPosition> jointCtrlPos {"Position"};
        PandaJointController<armarx::ControlTarget1DoFActuatorVelocity> jointCtrlVel {"Velocity"};
        PandaJointController<armarx::ControlTarget1DoFActuatorTorque> jointCtrlTor {"Torque"};
        PandaJointController<armarx::DummyControlTargetEmergencyStop> jointCtrlESt {"Velocity"};
        PandaJointController<armarx::DummyControlTargetStopMovement> jointCtrlMSt {"Velocity"};

    };

}
