/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::PandaKinematicSubUnit
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <mutex>

// Boost
#include <boost/algorithm/string.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/IceReportSkipper.h>
#include <RobotAPI/components/units/KinematicUnit.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/Units/RobotUnitSubUnit.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/util.h>

// PandaX
#include <ArmarXPandaBridge/libraries/PandaRT/NJointKinematicUnitMultiJointPassThroughController.h>


namespace pandax
{
    //TYPEDEF_PTRS_HANDLE(RobotUnit);

    TYPEDEF_PTRS_HANDLE(PandaKinematicSubUnit);
    class PandaKinematicSubUnit:
        virtual public armarx::RobotUnitSubUnit,
        virtual public armarx::KinematicUnit
    {

    public:

        struct ActuatorData
        {
            std::string name;
            std::size_t sensorDeviceIndex;
            NJointKinematicUnitMultiJointPassThroughControllerPtr ctrlPos;
            NJointKinematicUnitMultiJointPassThroughControllerPtr ctrlVel;
            NJointKinematicUnitMultiJointPassThroughControllerPtr ctrlTor;

            armarx::NJointControllerBasePtr getController(armarx::ControlMode c) const;
            armarx::NJointControllerBasePtr getActiveController() const;
        };
        PandaKinematicSubUnit();

        void setupData(
            std::string relRobFile,
            VirtualRobot::RobotPtr rob,
            std::map<std::string, ActuatorData>&& newDevs,
            std::vector<std::set<std::string> > controlDeviceHardwareControlModeGrps,
            std::set<std::string> controlDeviceHardwareControlModeGrpsMerged,
            armarx::RobotUnit* newRobotUnit
        );
        void update(const armarx::SensorAndControl& sc, const armarx::JointAndNJointControllers& c) override;

        // KinematicUnitInterface interface
        void requestJoints(const Ice::StringSeq&, const Ice::Current&) override;
        void releaseJoints(const Ice::StringSeq&, const Ice::Current&) override;

        void setJointAngles(const armarx::NameValueMap& angles, const Ice::Current&) override;
        void setJointVelocities(const armarx::NameValueMap& velocities, const Ice::Current&) override;
        void setJointTorques(const armarx::NameValueMap& torques, const Ice::Current&) override;
        void switchControlMode(const armarx::NameControlModeMap& ncm, const Ice::Current&) override;

        void setJointAccelerations(const armarx::NameValueMap&, const Ice::Current&) override;
        void setJointDecelerations(const armarx::NameValueMap&, const Ice::Current&) override;

        armarx::NameValueMap getJointAngles(const Ice::Current&) const override;
        armarx::NameValueMap getJointVelocities(const Ice::Current&) const override;
        std::vector<std::string> getJoints(const Ice::Current&) const override;

        armarx::NameControlModeMap getControlModes(const Ice::Current&) override;

        void onInitKinematicUnit()  override {}
        void onStartKinematicUnit() override {}
        void onExitKinematicUnit()  override {}

    private:

        std::map<std::string, ActuatorData> devs;
        // never use this when holding the mutex! (could cause deadlocks)
        armarx::RobotUnit* robotUnit = nullptr;
        mutable std::mutex dataMutex;
        armarx::NameControlModeMap ctrlModes;
        armarx::NameValueMap ang;
        armarx::NameValueMap vel;
        armarx::NameValueMap acc;
        armarx::NameValueMap tor;
        armarx::NameValueMap motorCurrents;
        armarx::NameValueMap motorTemperatures;
        armarx::NameStatusMap statuses;
        std::vector<std::set<std::string>> controlDeviceHardwareControlModeGroups;
        std::set<std::string> controlDeviceHardwareControlModeGroupsMerged;
        armarx::IceReportSkipper reportSkipper;
        std::vector<VirtualRobot::RobotNodePtr> sensorLessJoints;

        template<class ValueT, class SensorValueT, ValueT SensorValueT::* Member>
        static void UpdateNameValueMap(std::map<std::string, ValueT>& nvm, const std::string& name, const armarx::SensorValueBase* sv, bool& changeState)
        {
            const SensorValueT* s = sv->asA<SensorValueT>();
            if (!s)
            {
                return;
            };
            const ValueT& v = s->*Member;
            auto it = nvm.find(name) ;
            if (it == nvm.end() || it->second != v)
            {
                changeState = true;
            }
            if (it != nvm.end())
            {
                it->second = v;
            }
            else
            {
                nvm[name] = v;
            }
        }

    };

}
