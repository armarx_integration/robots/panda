/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointKinematicUnitMultiJointPassThroughController
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXPandaBridge/libraries/PandaRT/NJointKinematicUnitMultiJointPassThroughController.h>


using namespace armarx;
using namespace pandax;


NJointControllerRegistration<NJointKinematicUnitMultiJointPassThroughController> registrationControllerNJointKinematicUnitMultiJointPassThroughController("NJointKinematicUnitMultiJointPassThroughController");

NJointKinematicUnitMultiJointPassThroughController::NJointKinematicUnitMultiJointPassThroughController(
    RobotUnitPtr prov,
    const NJointKinematicUnitMultiJointPassThroughControllerConfigPtr& cfg,
    const VirtualRobot::RobotPtr&) :
    deviceNames(cfg->deviceNames),
    control(cfg->deviceNames.size())
{
    target.resize(cfg->deviceNames.size());
    sensor.resize(cfg->deviceNames.size());
    for (size_t i = 0; i < cfg->deviceNames.size(); i++)
    {
        ARMARX_DEBUG << "Device name is: " << i << " - " << cfg->deviceNames.at(i);
        control.at(i)  = 0.0;
        //        auto& c = control.at(i);
        //        c = 0.0;
        auto deviceName = cfg->deviceNames.at(i);
        const SensorValueBase* sv = useSensorValue(deviceName);

        std::string controlMode = cfg->controlMode;
        auto& sensor = this->sensor.at(i);
        auto& target = this->target.at(i);

        ControlTargetBase* ct = useControlTarget(deviceName, controlMode);
        //get sensor
        if (ct->isA<ControlTarget1DoFActuatorPosition>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorPosition>());
            sensor = &(sv->asA<SensorValue1DoFActuatorPosition>()->position);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorPosition>());
            target = &(ct->asA<ControlTarget1DoFActuatorPosition>()->position);
            sensorToControlOnActivateFactor = 1;
        }
        else if (ct->isA<ControlTarget1DoFActuatorVelocity>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorVelocity>());
            sensor = &(sv->asA<SensorValue1DoFActuatorVelocity>()->velocity);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorVelocity>());
            target = &(ct->asA<ControlTarget1DoFActuatorVelocity>()->velocity);
            ARMARX_INFO << deviceName << " velocity addr " << target;
            sensorToControlOnActivateFactor = 1;
            resetZeroThreshold = 1e5; // reset always
        }
        else if (ct->isA<ControlTarget1DoFActuatorTorque>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorTorque>());
            sensor = &(sv->asA<SensorValue1DoFActuatorTorque>()->torque);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorTorque>());
            target = &(ct->asA<ControlTarget1DoFActuatorTorque>()->torque);
            sensorToControlOnActivateFactor = 1;
            resetZeroThreshold = 1e5; // reset always
        }
        else
        {
            throw InvalidArgumentException {"Unsupported control mode: " + controlMode};
        }
    }
}
