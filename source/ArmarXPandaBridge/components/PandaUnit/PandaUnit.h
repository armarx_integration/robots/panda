/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXPandaBridge::ArmarXObjects::PandaUnit
 * @author     Mirko Wächter ( mirko dot waechter at kit dot edu )
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <chrono>

// Panda
#include <franka/robot.h>
#include <franka/robot_state.h>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>

// PandaX
#include <ArmarXPandaBridge/libraries/PandaRT/PandaKinematicSubUnit.h>


namespace pandax
{
    using ClockT = std::chrono::high_resolution_clock;

    //we can't store durations in atoms -> store nanoseconds
    inline std::uintmax_t nowNS()
    {
        return std::chrono::duration_cast<std::chrono::nanoseconds>(ClockT::now().time_since_epoch()).count();
    }

    template<class T>
    struct TripleBufferWithGuardAndTime
    {
        TripleBufferWithGuardAndTime(std::condition_variable* cv): cv {cv} {}

        std::unique_lock<std::recursive_mutex> guard()
        {
            return std::unique_lock<std::recursive_mutex> {m};
        }

        std::size_t getLastWriteT() const
        {
            return lastwriteNs;
        }
        bool isLastWrite1SecondAgo() const
        {
            return (lastwriteNs + 1000000000 < nowNS());
        }

        void reinit(const T& i)
        {
            tb.reinitAllBuffers(i);
        }

        T& getWriteBuffer()
        {
            return tb.getWriteBuffer();
        }
        const T& getReadBuffer() const
        {
            return tb.getReadBuffer();
        }
        void write()
        {
            auto g = guard();
            lastwriteNs = nowNS();
            tb.commitWrite();
            cv->notify_all();
        }
        void read()
        {
            tb.updateReadBuffer();
        }

    private:

        mutable std::recursive_mutex m;
        armarx::WriteBufferedTripleBuffer<T> tb;
        std::atomic<std::uintmax_t> lastwriteNs {0};
        std::condition_variable* cv;

    };

    class PandaJointDevice;
    TYPEDEF_PTRS_SHARED(PandaJointDevice);

    /**
     * @class PandaUnitPropertyDefinitions
     * @brief
     */
    class PandaUnitPropertyDefinitions :
        public armarx::RobotUnitPropertyDefinitions
    {

    public:

        PandaUnitPropertyDefinitions(std::string prefix) :
            armarx::RobotUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PandaIP", "10.6.6.1", "IP of the panda robot");

            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        }

    };

    /**
     * @defgroup Component-PandaUnit PandaUnit
     * @ingroup ArmarXPandaBridge-Components
     * A description of the component PandaUnit.
     *
     * @class PandaUnit
     * @ingroup Component-PandaUnit
     * @brief Brief description of class PandaUnit.
     *
     * Detailed description of class PandaUnit.
     */
    class PandaUnit :
        virtual public armarx::RobotUnit
    {

    public:

        PandaUnit():
            torquesTB {&cvGotSensorData},
            anglesTB {&cvGotSensorData},
            velocitiesTB {&cvGotSensorData},
            positionModeHash(hasher("Position")),
            velocityModeHash(hasher("Velocity")),
            torqueModeHash(hasher("Torque"))
        {}
        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

    protected:

        void onInitRobotUnit() override;
        void onConnectRobotUnit() override;
        void onDisconnectRobotUnit() override {}
        void onExitRobotUnit() override {}

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void initializeKinematicUnit() override;

        armarx::ManagedIceObjectPtr createPandaKinematicSubUnit(
            const Ice::PropertiesPtr& properties,
            const std::string& positionControlMode = armarx::ControlModes::Position1DoF,
            const std::string& velocityControlMode = armarx::ControlModes::Velocity1DoF,
            const std::string& torqueControlMode = armarx::ControlModes::Torque1DoF
        );
        void rtTask();

        void readPandaState(const franka::RobotState& state);
        void pandaCtrlLoop();

        void componentPropertiesUpdated(const std::set<std::string>& changedProperties) override;

    private:

        std::atomic_bool shutdownRtThread {false};
        std::thread rtThread, pandaThread;
        std::atomic_bool isPolling {false};

        std::string robotName;

        VirtualRobot::RobotPtr robot;

        armarx::KeyValueVector<std::string, PandaJointDevicePtr> jointDevs;
        TripleBufferWithGuardAndTime<std::array<double, 7>> torquesTB;
        TripleBufferWithGuardAndTime<std::array<double, 7>> anglesTB;
        TripleBufferWithGuardAndTime<std::array<double, 7>> velocitiesTB;
        armarx::NameValueMap velocitiesCtrl;
        armarx::NameValueMap anglesCtrl;
        armarx::NameValueMap torquesCtrl;

        std::condition_variable cvGotSensorData;
        /// @brief this timestamp is used for management so don't use virtual time here
        std::uintmax_t oputdatedSensorValuesTimepoint;
        IceUtil::Time sensorValuesTimestamp;

        //metadata
        std::atomic<std::uintmax_t> iterationCount {0};

        // franka members
        enum class PandaControlMode
        {
            None,
            Stop,
            Position,
            Velocity,
            Torque,
            CartesianPose,
            CartesianVelocity,
            NumModes
        };
        using PandaArray = std::array<double, 7>;
        std::unique_ptr<franka::Robot> frankaRobot;
        std::unordered_map<std::string, size_t> deviceNameToIndexMap;

        struct PandaControlState
        {
            PandaControlMode requestedCtrlMode = PandaControlMode::None;
            PandaArray positionTargets;
            PandaArray velocityTargets;
            PandaArray torqueTargets;
        };
        std::hash<std::string> hasher;
        const size_t positionModeHash, velocityModeHash, torqueModeHash;
        armarx::TripleBuffer<PandaControlState> pandaControlState;
        std::atomic<bool> callbackActive = false;

        // ModuleBase interface
        void rtPollRobotState();
        bool skipReport() const;
        void rtSendCommands();
        bool allNewData(bool printMissing = false) const;
        void fillTB(TripleBufferWithGuardAndTime<std::vector<float>>& b, const armarx::NameValueMap& nv, const std::string name) const;
        void rtUpdateSensors(bool wait);

    public:

        void joinControlThread() override
        {
            shutdownRtThread = true;
            rtThread.join();
        }

        IceUtil::Time getControlThreadTargetPeriod() const override
        {
            return IceUtil::Time::milliSeconds(1);
        }

    };
}
